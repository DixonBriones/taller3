/* •Transforme la hora actual en segundos.*/
var fechaHora = new Date();
var horas = fechaHora.getHours();
var minutos = fechaHora.getMinutes();
var segundos = fechaHora.getSeconds();
document.getElementById("reloj").innerHTML = horas+':'+minutos+':'+segundos;
document.getElementById("relojsegundos").innerHTML = ((horas*3600)+(minutos*60)+segundos);

/* Calcular el área de un triángulo, que es igual a: (base * altura)/2.*/

var form_area= document.getElementById("form_area");
var base =document.getElementById("base");
var altura=document.getElementById("altura");

form_area.addEventListener("submit", e=>{
    e.preventDefault();
    if(base.value.length!=0){
        if(altura.value.length!=0){ 
            var resultado_triangulo= (base.value*altura.value)/2;
            document.getElementById("area_resul").innerHTML = "El area resultante es :" +resultado_triangulo;   
        }else{
            alert("Debe ingresar la altura del triangulo");    
        }    
    }else{
        alert("Debe ingresar la base del triangulo");
    };

})
/* Calcule la raíz cuadrada de un número impar y muestre el resultado con 3 dígitos.*/
var form_raiz= document.getElementById("form_raiz");
var num_raiz =document.getElementById("num_raiz");

form_raiz.addEventListener("submit", e=>{
    e.preventDefault();
    if((num_raiz.value%2) !=0){
        var resultado_raiz=  Math.sqrt(num_raiz.value);
        document.getElementById("raiz_resul").innerHTML = "La raiz resultante es :" +resultado_raiz.toFixed(2);
    }else{
        alert("Debe ingresar un numero impar");
    };

})

/* Ingresar una cadena de texto y mostrar la longitud de la cadena.*/
var form_caracteres= document.getElementById("form_caracteres");
var caracteres =document.getElementById("caracteres");

form_caracteres.addEventListener("submit", e=>{
    e.preventDefault();
    if(caracteres.value.length!=0){
        var resultado_caracteres=  caracteres.value.length;
        document.getElementById("caract_resul").innerHTML = "La cantidad de caracteres es :" +resultado_caracteres;
    }else{
        alert("Debe ingresar un texto");
    };

})

/*Concatenar los arrays:  array1(Lunes, Martes, Miércoles, Jueves, Viernes) y array 2 (Sábado, Domingo)*/
var inicio_semana = new Array("Lunes", "Martes", "Miercoles", "Jueves" ,"Viernes");
var fin_semana = new Array("Sabado", "Domingo");
var array_concatenado = inicio_semana.concat(fin_semana);
document.getElementById("inicio_semana").innerHTML = inicio_semana;
document.getElementById("fin_semana").innerHTML = fin_semana;
document.getElementById("array_concatenado").innerHTML = array_concatenado;

/* Mostrar la versión del navegador.*/
var ver_nav = navigator.appVersion;
document.getElementById("ver_nav").innerHTML = ver_nav;

/* Mostrar el ancho y la altura de la pantalla.*/

var winancho = window.innerWidth;
var winalto = window.innerHeight;
document.getElementById("resolu_nav").innerHTML = winancho + " x "  + winalto;

/*Imprimir la página.*/

function btn_imprimir() {
    
      window.print();
    
  }